# car-rental

## Prerrequisitos

- Cuenta de Gmail.

## Preparar Ambiente

#### Notebook

Para la ejecución de todos los prerequisitos y scripts del ETL vamos a utilizar la herramienta Colab de Google, la cuál una herramienta para escribir y ejecutar código Python en la nube.

1. Una vez iniciada la sesión en tu cuenta de Gmail, ir al siguiente link [Colab](https://colab.research.google.com/drive/1yJDxTopx5D25aVIngixEir9WmC0fZxyE?usp=sharing)
2. Al dar click en la URL, se abrirá una nueva pestaña en el navegador y en ella se encontrará el entorno de trabajo que compartí.
3. Este  entorno  se  denomina  “Notebook”  y  puede  ser  almacenado  como  un archivo tanto en “Drive” como en el equipo local. La estructura del “Notebook” es la siguiente: 
    - En la parte superior se indica el nombre del archivo correspondiente. La extensión del archivo será ".ipnb ("Interactive Python NoteBook") 
    - Debajo se encuentra un menú desplegable con varias opciones 
    - Luego se halla una "celda" que es el lugar donde se puede escribir el texto o el código deseado. Una “celda” es la unidad mínima de ejecución dentro del "Notebook".
4. Ejecutar solo las celdas que tienen el ícono de "play" en orden consecutivo, es importante esperar a que cada una de ellas termine su ejecución, se podrá ver el resultado en una celda que se abrirá debajo.
![Play](./images/colab-play-icon.png)

NOTAS:

1. Algunas ejecuciones tendrán WARNINGS, ignorarlos.
2. Si se deja el Notebook inactivo por un tiempo se desconectará de la sesión y se tendrán que ejecutar de nuevo todas las celdas consecutivamente.
3. Cualquier modificación de las celdas podría alterar el resultado esperado, se adjunta el Notebook(car_rental.ipynb) en la carpeta /notebook del presente repo por si se requiere una ejecución desde Jupyter Notebook y para visualizar las ejecuciones que realicé anteriormente y los resultados de ellas.

