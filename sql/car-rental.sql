CREATE SCHEMA dwh;

SET search_path TO dwh;

CREATE TABLE IF NOT EXISTS dwh.dim_employee(
	pk_employee SERIAL PRIMARY KEY
	,employee_first_name VARCHAR(1000)
	,employee_last_name VARCHAR(1000)
	,employee_email VARCHAR(1000)
	,employee_gender VARCHAR(10)
	,employee_birth_dat DATE
	,is_active BOOLEAN  DEFAULT true
	,creation_date TIMESTAMP DEFAULT NOW()
	,update_date TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS dwh.dim_client (
    pk_client SERIAL PRIMARY KEY
	,client_full_name VARCHAR(100) NOT NULL
	,client_email VARCHAR(100) 
	,client_phone VARCHAR(100) 
	,client_birth_date DATE 
	,client_gender VARCHAR(1) 
	,city VARCHAR(100) 
	,country VARCHAR(100) 
	,postal_code VARCHAR(100) 
	,fiscal_identification_number1 VARCHAR(100) 
	,is_active BOOLEAN  DEFAULT false 
	,creation_date TIMESTAMP DEFAULT NOW()
	,update_date TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS dwh.dim_transaction (
    pk_transaction SERIAL PRIMARY KEY
	,transaction_type VARCHAR(150)
	,rate_per_day INTEGER
	,transaction_date TIMESTAMP WITHOUT TIME ZONE
	,transaction_amount INTEGER
	,payment_method VARCHAR(150)
	,duration_in_days INTEGER
	,creation_date TIMESTAMP DEFAULT NOW()
	,update_date TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS dwh.dim_booking_status_code (
    pk_booking_status_code SERIAL PRIMARY KEY
	,booking_status_description VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS dwh.dim_hub (
    pk_hub SERIAL PRIMARY KEY
	,location_name TEXT
	,region_name VARCHAR(128)
	,hub_name VARCHAR(128)
    ,hub_address TEXT
    ,hub_latitude TEXT
    ,hub_longitude TEXT
    ,postal_code VARCHAR(30)
	,creation_date TIMESTAMP DEFAULT NOW()
	,update_date TIMESTAMP DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS dwh.dim_car (
    pk_car SERIAL PRIMARY KEY
	,car_sku VARCHAR(256) 
	,car_year INTEGER 
	,brand_name VARCHAR(200)
	,model_name VARCHAR(200)
	,version_shortname VARCHAR(200) 
	,use_type VARCHAR(25)
	,km VARCHAR(25)
	,transmission VARCHAR(25)
	,fuel VARCHAR(25)
	,doors INTEGER
	,traction VARCHAR(20)
	,sunroof VARCHAR(25)
	,liters NUMERIC
	,gps VARCHAR(200)
	,vin VARCHAR(200)
	,availability_status BOOLEAN  DEFAULT true
);

CREATE TABLE IF NOT EXISTS dwh.fact_bookings (
    pk_booking SERIAL PRIMARY KEY
	,fk_booking_status_code INTEGER REFERENCES dwh.dim_booking_status_code (pk_booking_status_code)
	,fk_client INTEGER REFERENCES dwh.dim_client(pk_client)
	,fk_employee INTEGER REFERENCES dwh.dim_employee(pk_employee)
	,fk_hub_pickup INTEGER REFERENCES dwh.dim_hub(pk_hub)
	,fk_hub_return INTEGER REFERENCES dwh.dim_hub(pk_hub)
	,fk_car INTEGER REFERENCES dwh.dim_car(pk_car)
	,fk_transaction INTEGER REFERENCES dwh.dim_transaction(pk_transaction)
	,pickup_date TIMESTAMP WITHOUT TIME ZONE
	,return_date TIMESTAMP WITHOUT TIME ZONE
);

INSERT INTO dwh.dim_employee
(employee_first_name, employee_last_name, employee_email, employee_gender, employee_birth_dat, is_active)
VALUES
('Esteban', 'Restrepo', 'esteban.restrepo@hotmail.com', 'Male', '1990-11-29', true),
('Nelson', 'Nieves', 'nelson.nieves@hotmail.com', 'Male', '1983-01-04', true),
('Cassandra', 'Morales', 'cassandra.morales@gmail.com', 'Female', '1951-09-15', true),
('Denisse Guadalupe', 'Pichardo', 'denisse.pichardo@gmail.com', 'Female', '1967-04-04', true),
('Nathali', 'Lara', 'nathali.lara@yahoo.com', 'Female', '1977-10-13', true),
('Sukheil', 'Rodriguez', 'sukheil.rodriguez@yahoo.com', 'Male', '1993-10-12', true),
('David', 'Valente', 'david.valiente@gmail.com', 'Male', '1999-06-04', true),
('Daniel', 'Bazan', 'daniel.bazan@yahoo.com', 'Male', '1986-12-24', true),
('Natasha', 'Soto', 'natasha.soto@gmail.com', 'Female', '1967-12-29', true),
('Salvador', 'Hernandez', 'salvador.hernandez@hotmail.com', 'Male', '1991-12-10', true),
('Sonia', 'Alcazar', 'esteban.restrepo@hotmail.com', 'Female', '1990-11-29', true),
('Ricardo', 'Fraga', 'ricardo.fraga@gmail.com', 'Male', '1980-09-16', true),
('Alejandro', 'Ovando', 'ealejandro.ovando@hotmail.com', 'Male', '1978-05-15', true),
('Michel', 'Sanchez', 'michel.sanchez@yahoo.com', 'Male', '1966-03-16', true),
('Darwin', 'Denis', 'darwin.denis@yahoo.com', 'Male', '1990-10-21', true);

INSERT INTO dwh.dim_client
(client_full_name, client_email, client_phone, client_birth_date, client_gender, city, country, postal_code, fiscal_identification_number1, is_active)
VALUES
( 'EDUARDO TEODORO', 'toshujro425472@gmail.com', '3319257830', '1971-07-12', 'M', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'NAASW1GFJHJI3', true),
( 'IVAN LOPEZ', 'transmetal2134@yahoo.com', '551323445', '1977-03-06', 'M', 'LA MAGDALENA CONTRERAS', 'MX', '67890 ', 'GWHKM802348K31', true),
( 'ANTONIO JAIME', 'tnyjdffe@hotmail.com', '8120407461', '1957-03-17', 'M', 'COYOACAN', 'MX', '04918 ', 'MOOE878787E0', true),
( 'MIREYA GALICIA', 'toshiro425472@gmail.com', '3319257830', '1971-07-12', 'M', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'NAAS466HJI3', true),
( 'JOSE HIPOLITO SANCHEZ HERNANDEZ', 'pepepolos334z@yahoo.com', '5222261555', '1967-08-19', 'M', 'Heroica Puebla de Zaragoza', 'MX', '72590 ', 'SAFG911M82', true),
( 'JOSE DE LALLATA', 'tcanutin@hotmail.com', '5542855679', '1978-09-10', 'M', 'QUERETARO', 'MX', '76000 ', 'LLOJ780178UJ', true),
( 'BERTHA', 'ventas.rocio@hotmail.com', '+5233232341', '1951-12-12', 'F', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'BUVE722334MU8', true),
( 'CESAR VELÁZQUEZ', 'cole97@yahoo.com', '5211951223', '1941-05-23', 'M', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'JEDA033110L48', true),
( 'MAXIMINO MEJIA', 'seree333@gmail.com', '+90543668233', '1978-09-12', 'M', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'WUSWUZOFRLROFU', true),
( 'OMAR CORTEZ', 'omargzz2333@gmail.com', '3319257830', '1951-10-12', 'M', 'COYOACAN', 'MX', '04918 ', 'JLBLBASTAWR', true),
( 'MIREYA GALICIA', 'myeyasd69@gmail.com', '5657672633', '1971-11-12', 'F', 'COYOACAN', 'MX', '04918 ', 'CHAJIGOCOSU', true),
( 'JUAN MANUEL LOPEZ', 'surfboy57@gmail.com', '+5511568351732', '1967-08-17', 'M', 'QUERETARO', 'MX', '76000 ', 'PHODRLCHICU', true),
( 'OSCAR SANCHEZ', 'cibers.raffft@mail.com', '80948543085', '1956-12-12', 'M', 'QUERETARO', 'MX', '76000 ', 'XITLVEWRUWR', true),
( 'GERARDO LARA', 'geladio56@gmail.com', '58034958349', '1923-10-12', 'M', 'QUERETARO', 'MX', '76000 ', 'JAPRUXEFREW', true),
( 'IVONNE SOLIS', 'tezcertonur1007@gmail.com', '83092484309', '1978-11-12', 'F', 'COYOACAN', 'MX', '54700 ', 'TRAYEPHADRE', true);

INSERT INTO dwh.dim_transaction
(transaction_type, rate_per_day, transaction_date, transaction_amount, payment_method,duration_in_days)
VALUES
('payment', 560,'2011-01-01 13:30:00',1680,'cash',3),
('payment', 430,'2007-10-13 18:45:00',5590,'credit',7),
('payment', 660,'2009-03-05 17:37:00',6600,'cash',10),
('payment', 780,'2001-01-11 19:41:25',3120,'credit',4),
('payment', 666,'2013-06-03 02:36:10',1332,'debit',2),
('payment', 546,'2006-07-23 02:53:18',7644,'cash',14),
('payment', 330,'2009-10-15 13:05:44',1650,'transfer',5),
('payment', 534,'2013-03-23 04:26:40',4806,'debit',9),
('payment', 457,'2010-09-13 01:34:27',457,'debit',1),
('payment', 642,'2015-05-03 11:45:45',642,'credit',1),
('payment', 687,'2018-09-05 17:01:20',7557,'transfer',11),
('payment', 390,'2005-08-28 12:48:13',1170,'credit',3),
('payment', 1500,'2003-11-06 07:17:52',1500,'cash',1),
('payment', 944,'2016-09-28 22:00:08',1888,'transfer',2),
('payment', 1250,'2018-02-22 19:14:51',25000,'debit',20);

INSERT INTO dwh.dim_booking_status_code
(booking_status_description)
VALUES
('Cancelled'),
('Reserved'),
('Confirmed'),
('Completed');

INSERT INTO dwh.dim_hub
(location_name, region_name, hub_name, hub_address, hub_latitude, hub_longitude, postal_code)
VALUES
('Cinquentenário', 'Malvern','SHOPPING GRANJA VIANNA TEMPORAL','Maltepe Park Avm, Cevizli, Tugay Yolu Cd. No:67, 34846','32.50231788863051','-92.1564269810915','2495-448'),
('PortoBay Marquês', 'Abu Dhabi','WEWORK WT MORUMBI','Av. Iguatemi, 777 - Vila Brandina, Campinas - SP, 13092-902','30.658982127555657','104.07699465751648','2754-536'),
('The Wyche', 'Hounslow','CDMX - PORTAL CENTRO','Av. Corrientes 3247, C1193 CABA','54.321864545345306','-7.3255205154418945','3720-241'),
('Vidago Palace', 'Vidago','MORUMBI TOWN MATRIX','EST. DOS ALPES JARDIM BELVAL 06423-080 Brazi','51.52090719683514','-0.07790937063907677','4050-007'),
('B&B Hamm', 'Albufeira','MXUR - WH - LERMA 4','BH SHOPPING MATRIX Brazil','53.095480148710315','-2.313050164417973','4495-012'),
('Bienvenue', 'Whitehorse','CDMX - ANTARA FASHION HALL','Avenida Miguel Hidalgo Oriente No. 1350, Colonia Barrio San Sebastian, Municipio de Toluca, Estado de México.','51.480632999999997','-0.44665199999999999','5425-307'),
('Bleckmanns Hof', 'Safehaven','MXWA - FINANZAS','Av Fortuna 334, Magdalena de las Salinas, 07760, Ciudad de México, CDMX, México','51.515366967460054','-0.1610572636127472','8200-913'),
('Angra Marina', 'Hamm','MXBA - CDI - CONSTITUYENTES','Avenida Revolución 2703, Colonia Ladrillera, Monterrey, Nuevo León, CP: 64830','52.09148165852654','-2.33771413564682','9004-540'),
('Arkadenhof Kurtz', 'Mana Island','IGUATEMI ESPLANADA TEMPORAL','Av Constituyentes 980, Lomas Altas, 11950, CDMX, México','53.854021731535695','-9.295845773155179','5700'),
('B&D', 'Cascais','VERIFICENTRO','Perif. Sur 3720, Jardines del Pedregal, Álvaro Obregón, 01900 Ciudad de México, CDMX','46.32942633598124','11.235171050262466','30159'),
('The Connaught Inn', 'Fatima','EN TRANSITO - MTY GARANTIA','Anillo Vial Fray Junípero Serra 7901, La Purísima, 76146 Santiago de Querétaro, Qro.','42.99197860004979','11.359574870477331','39326'),
('Accra', 'Verbier','SUPER SHOPPING OSASCO','Calle Londres 189, Juárez, 06600, Ciudad de México, CDMX, México','38.71330766743014','-9.137616902589798','59065'),
('Sea Gull Hurghada', 'Twann','CDI - CONSTITUYENTES','Av. Padre José Maria, 300 - Santo Amaro, São Paulo - SP, 04753-060, Brasil','38.723847720173154','-9.150150865316391','59368'),
('Dighton', 'Hannover','MXBA - SANTA FE','Lorenzo Boturini 258, Centro, Cuauhtémoc, 06840 Ciudad de México, CDMX','39.629279544299216','-8.67097020149231','86825'),
('Reefs', 'Zell','Maltepe Park AVM','Maslak Atatürk Oto Sanayi Sitesi 1. Kısım 45. Sokak, Pilot Garage No: 34, 34398','38.70232774364404','-9.410842538272846','75009');

INSERT INTO dwh.dim_car
(car_sku, car_year, brand_name, model_name, version_shortname, use_type, km, transmission, fuel, doors, traction, sunroof, liters,  gps, vin,  availability_status)
VALUES
('B04P130326', 2013, 'Mini', 'Mini Cooper','4 pts. 320i Sport Line, TA, tela/piel, QC, bi-xenón, RA-18','Particular','38000','Manual','Gasolina',5,'4x2','unknown',1.6,'yes','2UMUSPLQUFR',true),
('C08P132302', 2016, 'Jeep', 'Grand Cherokee','5 pts. 35i Premium, L6, TA, 5 pas.','Particular','68500','Automática','Diesel',4,'4x2','unknown',3,'yes','8LCREJUFREB',false),
('F12P140102', 2010, 'Audi', 'Q3 Quattro','5 pts. Elite, 211HP, S Tronic, piel, RA-18','Particular','13000','Automática','Diesel',2,'4x2','unknown',2.9,'yes','Q6TRUQE0AHA',true),
('L25P131001', 2018, 'Audi', 'A1','3 pts. HB S Line, 185HP, S Tronic, climatronic, tela/piel, RA-17, QC','Particular','23000','Automática','Flex',2,'4x2','unknown',1.4,'no','TISTLFL7HEB',true),
('A03P152205', 2015, 'Honda', 'CR-V','5 pts. LX, TA, CD, RA','Particular','13800','Manual','Gasolina',5,'4x2','unknown',1.6,'no','VLTLCH0SWAN',true),
('V44P131703', 2020, 'Hyundai', 'Elantra',' pts. Limited Tech, 2.0l, TA, climatronic, piel, xenón, GPS,  RA-17','Particular','48500','Automática','Gasolina',4,'4x2','unknown',1.3,'yes','47575107093',true),
('T43P140703', 2019, 'Honda', 'Accord','2 pts. EX  Coupé, V6, TA, climatronic, CD,  QC, piel, GPS, f. niebla, RA-18','Particular','50100','Automática','Gasolina',4,'4x2','unknown',4.2,'no','25768012972',true),
('S42P130301', 2014, 'Suzuki', 'Grand Vitara','5 pts. GL, L4, TA, CD, f.niebla','Particular','47000','Automática','Flex',4,'4x2','unknown',2.9,'yes','ZIQOSA8RIFR',true),
('H17P140104', 2016, 'Toyota', 'Highlander','5 pts. Limited, TA, climatronic, piel, TP, GPS, RA-19','Particular','16014','Automática','Flex',2,'4x2','unknown',4.2,'no','1RLFROWO0IW',true),
('H19P170104', 2021, 'Volkswagen', 'Jetta A6','4 pts. Style, TM5, VE, CD,  RA-16','Particular','30000','Automática','Gasolina',5,'4x2','unknown',1.4,'yes','CREPI0RAPUK',true),
('H17P130401', 2022, 'Audi', 'Q3 Quattro','5 pts. Elite, 211HP, S Tronic, piel, GPS, RA-18','Particular','7500','Manual','Gasolina',5,'4x2','unknown',4.5,'yes','WLCHEZUX3QU',true),
('A03P120116', 2017, 'Land Rover', 'Range Rover Sport','5 pts. SportHSE, TA, piel, QC, RA-19','Particular','60000','Automática','Diesel',5,'4x2','unknown',1.6,'yes','QL2OTEY8CUT',false),
('A03P132205', 2012, 'Fiat', '500','3 pts. HB Sport, TM5, QC, alerón, f.niebla, RA-16','Particular','85000','Manual','Diesel',2,'4x2','unknown',2.9,'yes','G0F8ODUCHLR',true),
('F12P160102', 2011, 'Chevrolet', 'Spark','5 pts.  HB LT, TM5, a/ac., CD, R-14','Particular','83000','Manual','Diesel',4,'4x2','unknown',3,'no','00191453889',true),
('B04P111608', 2013, 'BMW', 'Serie 1','2 pts. 125i M Sport Coupé, piel, RA-17, TA','Particular','31000','Manual','Gasolina',4,'4x2','unknown',1.6,'no','53845337338',true);

INSERT INTO dwh.fact_bookings
(fk_booking_status_code, fk_client, fk_employee, fk_hub_pickup, fk_hub_return, fk_car, fk_transaction, pickup_date, return_date)
VALUES
(2, 6, 4, 5, 7, 3, 7, '2009-10-15 13:05:44', '2009-10-20 10:15:24'),
(1, 7, 2, 4, 2, 5, 14, '2016-09-28 22:00:08', '2016-09-30 10:00:08'),
(1, 4, 1, 11, 10, 6, 3, '2009-03-05 17:37:00', '2009-03-15 11:43:12'),
(2, 5, 10, 12, 11, 7, 5, '2013-06-03 02:36:10', '2013-06-05 09:12:31'),
(2, 2, 15, 14, 12, 8, 8, '2004-03-21 04:13:14', '2005-03-21 08:17:18'),
(2, 1, 9, 10, 13, 9, 9, '2013-03-23 04:26:40', '2013-03-31 12:43:12'),
(2, 10, 7, 1, 14, 12, 1, '2011-01-01 13:30:00', '2011-01-04 15:23:34'),
(2, 15, 6, 13, 15, 13, 11, '2018-09-05 17:01:20', '2018-09-26 19:23:14'),
(2, 3, 11, 1, 9, 14, 2, '2007-10-13 18:45:00', '2007-10-20 14:23:33'),
(2, 8, 12, 2, 8, 15, 15, '2018-02-02 19:14:51', '2018-02-22 08:54:51'),
(3, 11, 13, 15, 6, 10, 12, '2005-08-28 12:48:13', '2005-08-31 11:47:52'),
(3,14, 14, 12, 5, 11, 4, '2001-01-11 19:41:25', '2001-01-15 15:21:45'),
(4, 4, 15,11, 4, 2, 6, '2006-07-03 02:53:18', '2006-07-23 05:43:23'),
(4, 12, 3, 8, 3, 1, 10, '2015-05-03 11:45:45', '2015-05-04 10:32:23'),
(2, 13, 8, 6, 1, 7, 13, '2003-11-06 07:17:52', '2003-11-07 20:23:34');





